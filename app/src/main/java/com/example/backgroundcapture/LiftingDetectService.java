package com.example.backgroundcapture;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class LiftingDetectService extends Service implements SensorEventListener {

  private SensorManager mSensorManager;
  private Camera mCamera;
  private SurfaceHolder mSurfaceHolder;


  public LiftingDetectService() {
  }

  @Override//
  public int onStartCommand(Intent intent, int flags, int startId) {
    Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
    mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    //registering Sensor
    try {
      Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

      mSensorManager.registerListener(this,
          sensor,
          SensorManager.SENSOR_DELAY_UI
      );
      //then you should return sticky
    } catch (NullPointerException e) {
      Toast.makeText(getApplicationContext(), "cannot init accelerometer sensor", Toast.LENGTH_SHORT).show();
    }
    return Service.START_STICKY;
  }


  Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
    public void onPictureTaken(byte[] imageData, Camera c) {
      Log.e("Callback TAG", "Here in jpeg Callback");

      if (imageData != null) {
        FileOutputStream outputStream = null;
        try {
          File xFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "Image.jpg");

          outputStream = new FileOutputStream(xFile);
          outputStream.write(imageData);

          // Removed the finish call you had here
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          if (outputStream != null) try {
            outputStream.close();
          } catch (IOException ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
          }
        }

      }
    }
  };

  @Override
  public void onCreate() {
    mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    //add this line only
  }

  @Override
  public IBinder onBind(Intent intent) {
    // TODO: Return the communication channel to the service.
    throw new UnsupportedOperationException("Not yet implemented");
  }


  private double mAccelCurrent;
  private double mAccel = 0;
  private boolean mTriggered = false;
  private boolean mStable = false;

  @Override
  public void onSensorChanged(SensorEvent event) {
    try {
      if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
        float[] gravity = event.values.clone();
        // Shake detection
        float x = gravity[0];
        float y = gravity[1];
        float z = gravity[2];

        float yAbs = Math.abs(gravity[1]);
        String TAG = "X";

        double lastAccel = mAccelCurrent;
        mAccelCurrent = Math.sqrt(x * x + y * y + z * z);
        double delta = mAccelCurrent - lastAccel;
        mAccel = mAccel * 0.9f + delta;

        if (mAccel < 3.0) {
          mStable = true;
        }
        if (mAccel > 3.0 && !mTriggered && mStable) {
          try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();

            startCapture();
            mTriggered = true;

          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static Camera openFrontCamera() {
    int numberOfCameras = Camera.getNumberOfCameras();
    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
    for (int i = 0; i < numberOfCameras; i++) {
      Camera.getCameraInfo(i, cameraInfo);
      if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
        return Camera.open(i);
      }
    }
    return null;
  }

  private void startCapture() {
    if (mCamera == null) {
      mCamera = openFrontCamera();
    }
    SurfaceView surfaceView = new SurfaceView(getBaseContext());


    try {

      Camera.Parameters p = mCamera.getParameters();
      mCamera.setParameters(p);
      SurfaceTexture st = new SurfaceTexture(MODE_PRIVATE);
      mCamera.setPreviewTexture(st);
      mCamera.startPreview();

      mCamera.takePicture(null, null, mPictureCallback);

    } catch (Exception e) {
      e.printStackTrace();
    }


    //Get a surface
    mSurfaceHolder = surfaceView.getHolder();
    //tells Android that this surface will have its data constantly replaced
    mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }

}